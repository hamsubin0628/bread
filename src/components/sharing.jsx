import React from 'react';

function Sharing(queue1, queue2) {
    let sum1 = queue1.reduce((a, b) => a + b);
    let sum2 = queue1.reduce((a, b) => a + b);
    let count = 0;

    while (queue1.length > 0 && queue2.length > 0) {
        if (sum1 === sum2) {
            return count;
        } else if (sum1 > sum2) {
            let popValue = queue1.shift();
            queue2.push(popValue);
            sum1 -= popValue;
            sum2 += popValue;
        } else {
            let popValue = queue2.shift();
            queue1.push(popValue);
            sum1 += popValue;
            sum2 -= popValue;
        }
        count++;
    }
    return sum1 === sum2 ? count : -1;
}
